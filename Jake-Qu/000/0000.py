from PIL import Image, ImageDraw, ImageFont, ImageColor
def add_num(img):
    #创建一个Draw对象
    draw = ImageDraw.Draw(img)
    #创建一个 Font,定义字体和字体大小
    myfont = ImageFont.truetype('C:/windows/fonts/Arial.ttf', size=30)
    #定义字体颜色
    fillcolor = ImageColor.colormap.get('red')
    width, height = img.size
    #定义文字位置，要书写的文字
    draw.text((width - 35, 0), '66', font=myfont, fill=fillcolor)
    #返回结果
    img.save('result.jpg', 'jpeg')
    return 0
if __name__ == '__main__':
    image = Image.open('000.jpg')
    add_num(image)

