#!/usr/bin/env python
#coding: utf-8
import xlrd

# 存放文件的路径
filepath = './1.xls'

def run():
    # 打开表格
    xls = xlrd.open_workbook(filepath)
    sheet = xls.sheet_by_index(0)
    zhujiao = 0
    beijiao = 0
    # 遍历表格，查到类型为被叫或主叫的行，记录通话时长
    for i in range(1, sheet.nrows):
        rv = sheet.row_values(i)
        #print(rv)
        if rv[4] == '主叫':
            if "分" not in rv[3]:
                str = rv[3].replace('秒', '.')
                miao = int(str.split('.')[0])
                zhujiao += miao
            else:
                str = rv[3].replace('分', '.')
                str = str.replace('秒', '')
                fen = int(str.split('.')[0])
                miao = int(str.split('.')[1])
                zhujiao += (fen*60 + miao)
        elif rv[4] == '被叫':
            if "分" not in rv[3]:
                str = rv[3].replace('秒', '.')
                miao = int(str.split('.')[0])
                beijiao += miao
            else:
                str = rv[3].replace('分', '.')
                str = str.replace('秒', '')
                fen = int(str.split('.')[0])
                miao = int(str.split('.')[1])
                beijiao += (fen*60 + miao)
    print('主叫时长：%s分钟' % (zhujiao / 60))
    print('被叫时长：%s分钟' % (beijiao / 60))
    print('总通话时长：%s分钟' % ((zhujiao + beijiao) / 60))
    zong = round(((zhujiao + beijiao)/60),0)
    print("运营商最终统计为：%d分钟" %(zong))
run()


