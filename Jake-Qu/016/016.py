import os
import json
import xlwt

def read_txt(path):
    with open(path, 'r',encoding='utf-8') as f:
        text = f.read()
        text_json = json.loads(text)
    return text_json

def save_into_excel(content_dict, excel_name):
    wb = xlwt.Workbook()
    ws = wb.add_sheet("numbers",  cell_overwrite_ok=True)
    row = 0
    col = 0
    for i in content_dict:
        for k in i:
            ws.write(row, col, k)
            col += 1
        row += 1
        col = 0

    wb.save(excel_name)


if __name__ == "__main__":
    read_content = read_txt('number.txt')
    save_into_excel(read_content, 'number.xls')
