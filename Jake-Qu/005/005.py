import os
from PIL import Image
import re

start_path="./pic/"
iphone5_width=1136
iphone5_depth=640
list = os.listdir(start_path)
#print(list)
for pic in list:
    path=(start_path+pic)
    #print(path)
    images = Image.open(path)
    w, h = images.size
    #print(w,h)
    if w > iphone5_width:
        print("图片名称为" + pic + "图片被修改")
        w_new = iphone5_depth*w/h
        h_new = iphone5_depth
        out = images.resize((int(w_new),int(h_new)),Image.ANTIALIAS)

        new_pic=re.sub(pic[:-4],pic[:-4]+'_new',pic)
        out.save(start_path + new_pic, 'jpeg')
        print("新生成的图片名称：" + new_pic)
    else:
        print("图片"+ pic +"分辨率大小不变")
