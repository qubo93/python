#coding=utf-8
#**第 0006 题：**你有一个目录，放了你一个月的日记，都是 txt，为了避免分词的问题，假设内容都是英文，请统计出你认为每篇日记最重要的词。

import os
import re

def findWord(start_path):
    if not os.path.isdir(start_path):
        return
    list = os.listdir(start_path)
    reObj = re.compile('\b?(\w+)\b?')
    for diary in list:
        filePath = os.path.join(start_path, diary)
        if os.path.isfile(filePath) and os.path.splitext(filePath)[1] == '.txt':
            with open(filePath) as f:
                data = f.read()
                words = reObj.findall(data)
                wordDict = dict()
                for word in words:
                    word = word.lower()
                    if word in ['a', 'the', 'to']:
                        continue
                    if word in wordDict:
                        wordDict[word] += 1
                    else:
                        wordDict[word] = 1
            #print(wordDict)
            ansList = sorted(wordDict.items(), key=lambda t: t[1], reverse=True)
            #print(ansList)
            print('file: %s->the most word: %s' % (diary, ansList[0]))

findWord('./diary/')
