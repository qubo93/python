#coding=utf-8
#**第 0007 题：**有个目录，里面是你自己写过的程序，统计一下你写过多少行代码。包括空行和注释，但是要分别列出来。
import os
start_path = "./directory/"
list = os.listdir(start_path)
counts = dict()
for file in list:
    filePath = os.path.join(start_path, file)
    print(filePath+ ":")
    for line in open(filePath,'rb'):
        if line.startswith(b'#') or line.startswith(b"'"):
            if '注释' not in counts:
                counts['注释']  = 1
            else:
                counts['注释'] += 1
        elif line.startswith(b' '):
            if '空行' not in counts:
                counts['空行']  = 1
            else:
                counts['空行'] += 1
        else:
            if '代码' not in counts:
                counts['代码'] = 1
            else:
                counts['代码'] += 1
    for k,v in counts.items():
        print("代码中"+ k + "有" + str(v) +"行")
        counts = dict()