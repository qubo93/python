#coding=utf-8
import re
from collections import Counter
file = open('english_world.txt','r')
str = file.read()

reOBJ = re.compile('\b?(\w+)\b?')
words = reOBJ.findall(str)
worddirc = dict()

for word in words:
    if word.lower() in worddirc:
        worddirc[word.lower()] +=1
    else:
        worddirc[word] = 1
for key,value in worddirc.items():
    print('%s:%s' %(key,value))
