#coding=utf-8
import random
import pymysql
import time
import redis
time_value= time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))

def insert_value(code_value,time_value):
    pool = redis.ConnectionPool(host='192.168.1.186', port=6379)
    redis_obj = redis.Redis(connection_pool=pool)
    try:
        redis_obj.set(code_value,time_value)
    except:
        print("not ok!exit..")
        exit(1)
    else:
        print(code_value + " was inserted.")

def generate(num=1):
    n = 1
    while (n <= int(num)):
        generatse = (''.join(random.sample(['z', 'y', 'x', 'w', 'v', 'u', 't', 's', 'r', 'q', 'p', 'o', 'n', 'm', 'l', 'k', 'j', 'i', 'h', 'g', 'f', 'e','d', 'c', 'b', 'a', '1', '2', '3', '4', '5', '6', '7', '8', '9'], 12)))
        n += 1
        time_value1 = time_value
        yield generatse,time_value1

num = input('how many code do you want generate?')
result = generate(num)
for i,time_value1 in result:
    insert_value(i,time_value1)