import json
import xlwt
def readfile(file):
    with open(file, 'r', encoding='utf8') as f:
        data = json.load(f)
    return data

txtdata = readfile('city.txt')
book = xlwt.Workbook()  # 创建一个excel对象
sheet = book.add_sheet('sheet1', cell_overwrite_ok=True)  # 添加一个sheet页
for i in range(len(txtdata)):
    sheet.write(i,0,i + 1)
    jsondata = txtdata[str(i + 1)]
    sheet.write(i,1,txtdata[str(i + 1)])


book.save('city.xls')