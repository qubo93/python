import json
import xlwt
def readfile(file):
    with open(file, 'r', encoding='utf8') as f:
        data = json.load(f)
    return data

all = readfile('student.txt')
#print(a)
title = ["学号", "姓名", "语文成绩", "数学成绩", "英语成绩"]
book = xlwt.Workbook()  # 创建一个excel对象
sheet = book.add_sheet('sheet1', cell_overwrite_ok=True)  # 添加一个sheet页
for lie in range(len(title)):  # 循环列
    sheet.write(0, lie, title[lie])  # 将title数组中的字段写入到0行i列中
for line in all:  # 循环字典
    #print('line:', line)
    sheet.write(int(line), 0, line)  # 将line写入到第int(line)行，第0列中
    for i in range(len(all[line])):
        sheet.write(int(line), i + 1, all[line][i])
book.save('result.xls')
print("save excel success.")